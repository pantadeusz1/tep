#ifndef ___GAME_CLIENT_HPP________
#define ___GAME_CLIENT_HPP________
#include "game_all.hpp"

#include <chrono>
#include <string>

namespace game {

class game_client_t {
public:
  bool game_on;
  net::player_t local_player;
  net::addr_t server_addr;
  int client_socket; // socket

  std::map<int, net::player_t> players;

  std::uint64_t game_tick;
  std::map<uint64_t, game_state_t> game_state;
  std::map<uint64_t, game_events_t> events_history;
  std::chrono::steady_clock::time_point current_time;

  game_events_t get_local_events(const uint64_t timestamp,
                                 net::player_t player);

  game_events_t get_all_network_events();

  void send_events(const game_events_t &events);

  void handshake(std::string player_name, std::string hostname, int port);

  void goodbye();

  game_client_t(std::string player_name, std::string hostname, int port);

  bool do_time();
  void game_loop_start();
  game::game_state_t game_loop_iteration();
};

} // namespace game

#endif
