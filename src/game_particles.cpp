
#include "game_particles.hpp"

namespace game {
    
  particle_t::particle_t() {
    static uint64_t next_id = 0;
    p = {0, 0};
    v = {0, 0};
    ttl = 0;
    id = next_id++;
    owner = 0;
  };

  /**
   * @brief
   *
   * @param dt
   * @return particle_t
   */
  particle_t particle_t::update(const double dt) const {
    particle_t updated = *this;
    updated.p = p + v * dt + a * dt * dt / 2.0;
    updated.v = v + a * dt;
    updated.a = a;
    updated.ttl = ttl - 1;
    return updated;
  }

std::vector<particle_t>
particle_t::update_particle_ts(std::vector<particle_t> const &particle_ts, double dt) {
  std::vector<particle_t> ret_particle_ts;
  for (auto p : particle_ts) {
    auto new_p = p.update(dt);
    if (new_p.ttl > 0)
      ret_particle_ts.push_back(new_p);
  }
  return ret_particle_ts;
}

std::vector<particle_t> particle_t::generate_explosion(position_t p, int owner, int n,
                                           double power, position_t accel,
                                           int max_ttl, std::mt19937 &random_generator) {
  std::normal_distribution<double> distrib;
  std::uniform_real_distribution<double> angle_distr;
  std::uniform_int_distribution<int> ttl_distribution(1, std::max(2, max_ttl));
  std::vector<particle_t> ret;
  double a0 = angle_distr(random_generator) * M_PI * 2;
  for (int i = 0; i < n; i++) {
    particle_t particle;
    particle.p = p;
    double v0 = std::abs(distrib(random_generator) * power);
    double a = a0 + angle_distr(random_generator) * M_PI * 2.0;
    particle.v = {std::cos(a) * v0, std::sin(a) * v0};
    particle.a = accel;
    particle.ttl = ttl_distribution(random_generator);
    particle.owner = owner;
    ret.push_back(particle);
  }
  return ret;
}

}
