#ifndef __GAME_ALL__HPP______
#define __GAME_ALL__HPP______

#include <random>

#include "game_all.hpp"
#include "game_particles.hpp"
#include "net_common.hpp"
#include <list>
#include <map>
namespace game {

using game_events_t = std::list<net::message_t>;

class game_state_t {
 public:
  std::mt19937 random_generator;
  uint64_t game_tick;

  std::vector<game::particle_t> particle_ts;

  game_state_t() = default;

  game_state_t(uint64_t game_tick_, uint32_t seed0) ;

  static game_state_t calculate_new_game_state(const game_state_t &gs,
                              const game_events_t &events) ;
};


}  // namespace game

#endif
