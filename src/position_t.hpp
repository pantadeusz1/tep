#ifndef ___POSITION_T___TP____
#define ___POSITION_T___TP____

//namespace tp {

struct position_t {
  double y, x;
  double &operator[](unsigned int i) { return (i & 1) ? x : y; }
  double operator[](unsigned int i) const { return (i & 1) ? x : y; }
};

inline position_t operator+(const position_t &a, const position_t &b) {
  return {a[0] + b[0], a[1] + b[1]};
}
inline position_t operator-(const position_t &a, const position_t &b) {
  return {a[0] - b[0], a[1] - b[1]};
}
inline position_t operator*(const position_t &a, const double &b) {
  return {a[0] * b, a[1] * b};
}
inline position_t operator/(const position_t &a, const double &b) {
  return {a[0] / b, a[1] / b};
}

// inline position_t next_position(position_t p, position_t v, position_t a, double dt) {
//   return p + v * dt + a * dt * dt / 2.0;
// }
// 
// inline position_t next_velocity( position_t v, position_t a, double dt) {
//   return v + a * dt;
// }

// }

#endif
