#ifndef ___GAME_SERVER_HPP______
#define ___GAME_SERVER_HPP______

#include <map>
#include <tuple>

#include "game_all.hpp"
#include "net_common.hpp"
#include "net_messages.hpp"

namespace game {

class game_server_t {
 public:
 std::mt19937 random_generator;
  int udp_socket;

  uint64_t game_tick;

/**
 * @brief {id, {player, address}}
 */
  std::map<uint16_t, std::pair<net::player_t, net::addr_t>> players;

  game_server_t(int port);

  virtual ~game_server_t();

  int find_free_player_id();

  static void handle_payer_t(game_server_t *pthis, net::message_t message,
                             net::addr_t addr);

  static void handle_explosion_t(game_server_t *pthis, net::message_t message,
                                 net::addr_t from_addr);

  void handle_incoming_message(net::message_t message, net::addr_t addr);
  int data_exchange();

  int run();
};
}  // namespace game

#endif
