#include "game_all.hpp"

namespace game {

game_state_t::game_state_t(uint64_t game_tick_, uint32_t seed0) {
  game_tick = game_tick_;

  random_generator.seed(seed0);
};

game_state_t game_state_t::calculate_new_game_state(const game_state_t &gs,
                                       const game_events_t &events) {
  game_state_t game_state = gs;

  for (auto event : events) {
    // && (event.explosion.game_tick == gs.game_tick)
    if ((event.type == net::MESSAGE_EXPLOSION)) {
      auto explosion = event.explosion;
      auto new_explosion = game::particle_t::generate_explosion(
          explosion.p, explosion.id, 500, 100 * explosion.power + 1, {0, 20},
          200, game_state.random_generator);
      game_state.particle_ts.insert(game_state.particle_ts.end(),
                                    new_explosion.begin(), new_explosion.end());
    }
  }
  game_state.particle_ts =
      game::particle_t::update_particle_ts(game_state.particle_ts, net::dt);
  game_state.game_tick++;
  return game_state;
};

} // namespace game
