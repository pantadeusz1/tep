#ifndef ___GAME_PARTICLES__HPP__XXX___
#define ___GAME_PARTICLES__HPP__XXX___

#include "net_messages.hpp"
#include <random>

namespace game {

struct particle_t {
  position_t p;  // pozycja
  position_t v;  // predkosc
  position_t a;  // przyspieszenie
  uint64_t id;

  double ttl;  // czas zycia

  int owner;

  particle_t();

  particle_t update(const double dt) const;

  static std::vector<particle_t> update_particle_ts(
      std::vector<particle_t> const &particle_ts, double dt);

  static std::vector<particle_t> generate_explosion(
      position_t p, int owner, int n, double power, position_t accel,
      int max_ttl, std::mt19937 &random_generator);
};
}  // namespace engine

#endif
