#ifndef __NET_COMMON_TEP_HPP____
#define __NET_COMMON_TEP_HPP____

#include <list>
#include <netdb.h>
#include <string>
#include <tuple>
//#include <stdio.h>
//#include <string.h>
//#include <unistd.h>
//#include <arpa/inet.h>
//#include <stdlib.h>
//#include <fcntl.h>

namespace net {
using addr_t = std::pair<struct sockaddr_storage, socklen_t>;

int bind_socket(unsigned int port_nr);

std::pair<int,int> auto_bind_socket(int initial_port, int tries);

std::list<addr_t> find_addresses(const std::string addr_txt, const int port,
                                 int ai_family = AF_UNSPEC);
} // namespace net

#endif
