#include <SDL.h>

#include <any>
#include <array>
#include <chrono>
#include <functional>
#include <iostream>
#include <list>
#include <map>
#include <random>
#include <thread>
#include <tuple>
#include <vector>

#include "game_all.hpp"
#include "game_client.hpp"
#include "game_particles.hpp"
#include "game_server.hpp"
#include "net_common.hpp"
#include "net_messages.hpp"
#include "tp_args.hpp"

using namespace game;

/**
 * @brief Graphics engine implementation
 *
 * it requires knowledge of game_t class
 *
 */
class graphics_t {
  SDL_Window *window;
  SDL_Renderer *renderer;

public:
  graphics_t() {
    SDL_Init(SDL_INIT_EVERYTHING);
    SDL_CreateWindowAndRenderer(800, 600, SDL_WINDOW_RESIZABLE, &window,
                                &renderer);
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, 0);
    SDL_RenderSetLogicalSize(renderer, 320, 200);
  }

  virtual ~graphics_t() {
    SDL_DestroyWindow(window);
    SDL_Quit();
  }

  void draw(const game_state_t &game_state) {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);
    for (auto particle_t : game_state.particle_ts) {
      SDL_SetRenderDrawColor(
          renderer,
          std::min((int)255,
                   (int)particle_t.ttl * ((1 + particle_t.owner) % 8)),
          std::min((int)255,
                   (int)particle_t.ttl * ((3 + particle_t.owner) % 8)),
          std::min((int)255,
                   (int)particle_t.ttl * ((6 + particle_t.owner) % 8)),
          255);
      SDL_RenderDrawPoint(renderer, particle_t.p[0], particle_t.p[1]);
    }
    SDL_RenderPresent(renderer);
  }
};

int run_game_server(int port) {
  game_server_t server(port);

  return server.run();
}

int run_game_client(std::string player_name, std::string hostname, int port) {
  game_client_t game_client(player_name, hostname, port);

  using namespace std::chrono;
  using namespace std;
  graphics_t graphics;

  game_client.game_loop_start();
  while (game_client.game_on) {
    auto last_game_state = game_client.game_loop_iteration();
    auto skip_frame = game_client.do_time();
    // graphics
    if (!skip_frame) {
      graphics.draw(last_game_state);
    }
  }
  // send goodbye message
  game_client.goodbye();
  return 0;
}

int main(int argc, char *argv[]) {
  using namespace tp::args;
  auto help = arg(argc, argv, "help", false, "show help screen");
  auto server = arg(argc, argv, "server", false, "runs in server mode");
  auto client = arg(argc, argv, "client", false, "runs in client mode");
  auto host =
      arg(argc, argv, "host", std::string("127.0.0.1"), "server address");
  auto port = arg(argc, argv, "port", 12312, "server port");
  auto player_name = arg(argc, argv, "player_name", std::string("nameless"),
                         "set player name - must be unique");
  if (help) {
    std::cout << "Simple client-server demo on UDP protocol" << std::endl;
    args_info(std::cout);
    return 0;
  }
  if (server) {
    return run_game_server(port);
  } else if (client) {
    return run_game_client(player_name, host, port);
    //        return run_game_main(argc, argv);
  }
}
