#include "net_common.hpp"

#include <stdexcept>

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

namespace net {

int bind_socket(unsigned int port_nr) {
  if (port_nr <= 0)
    port_nr = 9922;
  int client_socket = socket(AF_INET6, SOCK_DGRAM, 0);
  if (client_socket < 0)
    throw std::invalid_argument("could not create socket");
  const int off = 0;
  if (setsockopt(client_socket, IPPROTO_IPV6, IPV6_V6ONLY, &off, sizeof(off))) {
    close(client_socket);
    throw std::invalid_argument("could not set options for IPV6_V6ONLY off");
  }
  { // set nonblocking
    int flags = fcntl(client_socket, F_GETFL, 0);
    fcntl(client_socket, F_SETFL, flags | O_NONBLOCK);
  }
  struct sockaddr_in6 addr;
  bzero(&addr, sizeof(addr)); // bind every available interface
  addr.sin6_family = AF_INET6;
  addr.sin6_port = htons(port_nr);
  socklen_t alen = sizeof(addr);
  if (bind(client_socket, (struct sockaddr *)(&addr), alen)) {
    close(client_socket);
    throw std::invalid_argument("could not bind selected address");
  }
  return client_socket;
}
/*

// operator that allows for comparing two adresses.
bool operator<(const addr_t & a, const addr_t &b) {
    uint64_t aa = 0,bb=0;
    for (int i = 0 ; i < a.second; i++) {
        aa = (aa << 4)+((char *)&(a.first))[i];
    }
    for (int i = 0 ; i < b.second; i++) {
        bb = (bb << 4)+((char *)&(b.first))[i];
    }
    return aa < bb;
}
*/

std::list<addr_t> find_addresses(const std::string addr_txt, const int port,
                                 int ai_family) {
  const std::string port_txt = std::to_string(port);
  struct addrinfo hints;
  std::list<addr_t> ret_addresses;
  bzero((char *)&hints, sizeof(struct addrinfo));
  hints.ai_family = ai_family;
  hints.ai_socktype = SOCK_DGRAM;

  struct addrinfo *addr_p;
  int err = getaddrinfo(addr_txt.c_str(), port_txt.c_str(), &hints, &addr_p);
  if (err) {
    throw std::invalid_argument("getaddrinfo error: " + std::to_string(err));
  }
  for (struct addrinfo *rp = addr_p; rp != NULL; rp = rp->ai_next) {
    struct sockaddr_storage a;
    socklen_t l;
    if (rp->ai_addrlen < sizeof(a)) {
      memcpy(&a, rp->ai_addr, rp->ai_addrlen); // warning!
      l = rp->ai_addrlen;
      ret_addresses.push_back({a, l});
    }
  }
  freeaddrinfo(addr_p);
  return ret_addresses;
}
std::pair<int, int> auto_bind_socket(int initial_port, int tries) {
  int client_socket = -1;
  for (int myport = initial_port; myport < (myport + tries); myport++) {
    try {
      client_socket = net::bind_socket(myport);
      return {client_socket, myport};
    } catch (std::invalid_argument &e) {
    }
  }
  return {-1, -1};
}

} // namespace net
