#include "game_server.hpp"
#include <unistd.h>
#include <iostream>
#include <chrono>
#include <thread>

namespace game {

game_server_t::game_server_t(int port) {
  random_generator.seed((std::random_device())());
  udp_socket = net::bind_socket(port);
  game_tick = random_generator();
}
 game_server_t::~game_server_t() { close(udp_socket); }

int game_server_t::find_free_player_id() {
  static int last_id = 0;
  return ++last_id;
}

 void game_server_t::handle_payer_t(game_server_t *pthis,
                                          net::message_t message,
                                          net::addr_t addr) {
  // send back our introduction
  net::player_t value = message.player;
  std::cerr << "hello or goodbye message.... " << value.id << " "
            << value.player_name << std::endl;
  if (value.id >= 0) {
    /// goodbye message
    if (pthis->players.count(value.id)) {
      pthis->players.erase(value.id);
      std::cerr << "goodbye message>>>> " << value.id << " "
                << value.player_name << std::endl;
    } else {
      std::cerr << "player not found but wants to disconnect" << std::endl;
    }
  } else {
    // hello message
    value.id = pthis->find_free_player_id();
    value.game_tick = pthis->game_tick;
    pthis->players[value.id] = {value, addr};
    message = value.m();
    sendto(pthis->udp_socket, message.data, sizeof(message.data), 0,
           (struct sockaddr *)&addr.first, addr.second);
    std::cerr << "hello message>>>> " << value.id << " " << value.player_name
              << std::endl;
    for (auto [addr, player] : pthis->players) {
      std::cout << "    PLAYER: " << player.first.id << " : "
                << player.first.player_name << std::endl;
    }
  }
}

 void game_server_t::handle_explosion_t(game_server_t *pthis,
                                              net::message_t message,
                                              net::addr_t from_addr) {
  int from_id = pthis->players[message.explosion.id].first.id;
  if (message.explosion.id != from_id) {
    std::cout << "ids don't match" << std::endl;
    return;
  }
  for (auto [id, player] : pthis->players) {
    if (message.explosion.id != from_id)
      std::cerr << "somehow the from_id " << from_id
                << " is different than that in explosion "
                << message.explosion.id << "..." << std::endl;
    message.explosion.id = from_id;
    std::cout << "   explosion to " << player.first.id << " from " << from_id
              << " serverTick: " << pthis->game_tick
              << " clientTick: " << message.explosion.game_tick << std::endl;
    auto addr = player.second;
    sendto(pthis->udp_socket, message.data, sizeof(message.data), 0,
           (struct sockaddr *)&addr.first, addr.second);
  }
}

void game_server_t::handle_incoming_message(net::message_t message,
                                            net::addr_t addr) {
  if (net::MESSAGE_PLAYER == message.type) {
    handle_payer_t(this, message, addr);
  } else if (net::MESSAGE_EXPLOSION == message.type) {
    handle_explosion_t(this, message, addr);
  } else {
    std::cerr << "unknown message...." << message.type << std::endl;
  }
}

int game_server_t::data_exchange() {
  while (true) {
    net::addr_t addr = {{}, sizeof(struct sockaddr_storage)};
    net::message_t buffer;
    ssize_t rsize =
        recvfrom(udp_socket, buffer.data, sizeof(buffer.data), MSG_DONTWAIT,
                 (struct sockaddr *)&addr.first, &addr.second);
    if (rsize < 128) break;  // get all messages.

    handle_incoming_message(buffer, addr);
  }
  // send tick info if needed
  {
    std::uniform_real_distribution<double> distr;
    if (distr(random_generator) < 0.1) {  // send all tick info
      net::tick_t tick;
      tick.game_tick = game_tick;
      auto message = tick.m();
      for (auto &[id, player] : players) {
        auto addr = player.second;
        sendto(udp_socket, message.data, sizeof(message.data), 0,
               (struct sockaddr *)&addr.first, addr.second);
      }
    }
  }
  return 0;
}

int game_server_t::run() {
  using namespace std::chrono_literals;
  using namespace std::chrono;
  using namespace std;
  game_tick = 0;
  std::chrono::steady_clock::time_point current_time =
      std::chrono::steady_clock::now();
  while (true) {
    data_exchange();  // data exchange is less frequent than the players ticks
    auto next_time = current_time +
                     microseconds((long long int)(net::server_dt * 1000000.0));
    if (next_time > std::chrono::steady_clock::now()) {
      std::this_thread::sleep_until(next_time);
    } else {
      std::cerr << "no delay" << std::endl;
    }
    current_time = next_time;
    game_tick += net::server_dt_multiply;
  }
}

}  // namespace game