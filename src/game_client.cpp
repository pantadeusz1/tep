#include "game_client.hpp"
#include <SDL.h>
#include <chrono>
#include <iostream>
#include <map>
#include <thread>

namespace game {

auto clear_events_history = [](uint64_t game_tick, auto &history) {
  std::list<uint64_t> history_to_del;
  for (auto &[t, v] : history) {
    if (t < game_tick)
      history_to_del.push_back(t);
  }
  for (auto t : history_to_del)
    history.erase(t);
};

game_events_t game_client_t::get_local_events(const uint64_t timestamp,
                                              net::player_t player) {
  SDL_Event e;
  static std::map<uint64_t, uint64_t> button_down_moments;

  game_events_t ret_events;
  // ret_events.quit = false;
  while (SDL_PollEvent(&e) != 0) {
    if (e.type == SDL_QUIT) {
      net::end_game_t q;
      q.game_tick = timestamp;
      q.quit = true;
      ret_events.push_back(q.m());
    } else if (e.type == SDL_KEYDOWN) {
    } else if (e.type == SDL_KEYUP) {
    } else if (e.type == SDL_MOUSEBUTTONDOWN) {
      button_down_moments[e.button.button] = e.button.timestamp;
    } else if (e.type == SDL_MOUSEBUTTONUP) {
      net::explosion_t explosion;
      std::cout << e.button.x << " " << e.button.y << std::endl;
      explosion.p = {(double)e.button.x, (double)e.button.y};
      explosion.power = (((double)(e.button.timestamp -
                                   button_down_moments[e.button.button])) /
                         1000.0);
      explosion.id = player.id;
      explosion.type = net::MESSAGE_EXPLOSION;
      explosion.game_tick = timestamp;
      ret_events.push_back({explosion : explosion});
    }
  }
  return ret_events;
}
game_events_t game_client_t::get_all_network_events() {
  game_events_t events;
  ssize_t rsize = 0;
  net::message_t buf;

  while ((rsize = recvfrom(client_socket, buf.data, sizeof(buf.data), 0, NULL,
                           0)) == sizeof(net::message_t)) {
    events.push_back(buf);
  }
  return events;
}
void game_client_t::send_events(const game_events_t &events) {
  auto [s_addr, s_len] = server_addr;
  for (auto buf : events)
    if (buf.type == net::MESSAGE_EXPLOSION) {
      sendto(client_socket, buf.data, sizeof(buf.data), 0,
             (struct sockaddr *)&s_addr, s_len);
    }
}

void game_client_t::handshake(std::string player_name, std::string hostname,
                              int port) {
  using namespace std::chrono_literals;
  client_socket = net::auto_bind_socket(port + 1, 128).first;
  if (client_socket == -1)
    throw std::invalid_argument("cannot bind port");
  auto addresses = net::find_addresses(hostname, port);
  std::cout << "had adresses, now try to connect" << std::endl;
  for (auto [s_addr, s_len] : addresses) {
    strcpy(local_player.player_name, player_name.c_str());
    local_player.id = -1;
    local_player.type = net::MESSAGE_PLAYER;
    net::message_t buf;
    buf.player = local_player;
    sendto(client_socket, buf.data, sizeof(buf.data), 0,
           (struct sockaddr *)&s_addr, s_len);

    std::cout << "hello getting.." << std::endl;
    struct sockaddr_storage peer_addr;
    socklen_t peer_addr_len = sizeof(struct sockaddr_storage);
    for (int i = 0; i < 300; i++) {
      ssize_t rsize = recvfrom(client_socket, buf.data, sizeof(buf.data), 0,
                               (struct sockaddr *)&peer_addr, &peer_addr_len);
      if (rsize == 128)
        break;
      std::this_thread::sleep_for(10ms);
    }
    local_player = buf.player;
    if ((local_player.id != -1) && (local_player.type == net::MESSAGE_PLAYER)) {
      std::cout << "connected: " << local_player.id << " "
                << local_player.player_name << std::endl;
      server_addr = {s_addr, s_len};
      break;
    } else {
      std::cout << "could not connect... timeout" << std::endl;
    }
  }
}

void game_client_t::goodbye() {
  if (local_player.id != -1) {
    auto [s_addr, s_len] = server_addr;
    net::message_t buf = {player : local_player};
    sendto(client_socket, buf.data, sizeof(buf.data), 0,
           (struct sockaddr *)&s_addr, s_len);
  }
}

void progress_game_state(std::map<uint64_t, game_state_t> &game_state,
                         std::map<uint64_t, game_events_t> &events_history,
                         std::uint64_t &game_tick,
                         game_events_t &network_events) {
  // find what is the oldest net event that we must process based on the
  // network events from server
  uint64_t oldest_net_event = game_tick;
  for (auto e : network_events) {
    if (e.type == net::MESSAGE_EXPLOSION) {
      uint64_t t = e.explosion.game_tick;
      events_history[t].push_back(e);
      oldest_net_event = std::min(t, oldest_net_event);
    } else if (e.type == net::MESSAGE_TICK) {
      if (game_tick != e.tick.game_tick)
        std::cout << "Server and Client tick don't match. Fix" << std::endl;
      game_tick = e.tick.game_tick; // synchronize to server :)
    }
  }

  // find the oldest known state from the state history
  uint64_t oldest_known_state = game_tick;
  for (auto &[t, v] : game_state) {
    if (t < oldest_known_state)
      oldest_known_state = t;
  }

  // if the oldest known state is latest than the oldest net event, we can
  // assume that the game just started and we must init the state of the game
  // for previous state
  if (oldest_known_state > oldest_net_event)
    game_state[oldest_net_event] =
        game_state_t(oldest_net_event, oldest_net_event);
  // calculate the physics. We replay the events from the oldest on the list
  // from server to the latest.
  for (uint64_t tick = oldest_net_event; tick <= game_tick; tick++) {
    game_state[tick].random_generator.seed(tick);
    game_state[tick + 1] = game_state_t::calculate_new_game_state(
        game_state[tick], events_history[tick]);
  }
  // make sure we don't store too much data in the memory
  clear_events_history(game_tick - net::events_history_size, events_history);
  clear_events_history(game_tick - net::events_history_size, game_state);
}

game_client_t::game_client_t(std::string player_name, std::string hostname,
                             int port) {
  handshake(player_name, hostname, port);

  game_tick = local_player.game_tick;
  game_state = {{game_tick, game_state_t(game_tick, game_tick)}};
  events_history = {};
}
bool game_client_t::do_time() {
  using namespace std::chrono_literals;
  using namespace std::chrono;
  bool skip_frame = false;
  auto next_time =
      current_time + microseconds((long long int)(net::dt * 1000000.0));
  if (next_time < std::chrono::steady_clock::now()) {
    skip_frame = true;
  } else {
    std::this_thread::sleep_until(next_time);
    skip_frame = false;
  }
  current_time = next_time;
  return skip_frame;
}

void game_client_t::game_loop_start() {
  game_on = true;
  current_time = std::chrono::steady_clock::now();
}

game::game_state_t game_client_t::game_loop_iteration() {
  game_tick++;
  // zdarzenia
  auto events = get_local_events(game_tick, local_player);
  for (auto e : events) {
    if (e.type == net::MESSAGE_END_GAME) {
      game_on = false;
      std::cerr << "close game" << std::endl;
    }
  }

  if (local_player.id != -1) {
    send_events(events);
    events = get_all_network_events();
  }

  progress_game_state(game_state, events_history, game_tick, events);

  return game_state[game_tick + 1];
}

} // namespace game
